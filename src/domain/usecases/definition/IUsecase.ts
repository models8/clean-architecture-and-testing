export interface IUsecase {
  execute<ReqDto, ResDto>(requestDTO: ReqDto | any): Promise<ResDto | any>;
}
