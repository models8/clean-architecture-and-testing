export type IResponseDto = Array<{
  acc_number: string;
  amount: string;
  transactions: Array<{
    label: string;
    amount: string;
    currency: string;
  }>;
}>
