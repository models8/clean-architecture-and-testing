import { IUsecase } from "../definition/IUsecase"
import { IResponseDto } from "./IResponseDto"
import {
  getAccounts,
  getAccountTransactions,
  getAllAccounts,
  getAllAccountsTransactions
} from "../../../dataproviders/apis/bridgeapi.io/accounts"
import { IAccount as IAccountDataprovider } from "../../../dataproviders/apis/bridgeapi.io/definitions/IAccountsResponse"
import { Account } from "../../entities/Account"
import { ITransaction as ITransactionProvider } from "../../../dataproviders/apis/bridgeapi.io/definitions/IAcountsTransactionResponse"
import { Result } from "../../../utility/Result"
import { mapAccounDto } from "../../mappers/mapAccounDto"
import { mapTransactionDto } from "../../mappers/mapTransactionDto"

export class GetAllAccounts implements IUsecase {
  async execute (): Promise<IResponseDto> {
    const getAccountResult: Result<IAccountDataprovider[], string> =
      await getAllAccounts(getAccounts)

    if (!getAccountResult.success)
      throw new Error("Error during account extraction")

    const accountEntities: Account[] = []
    for (const accountDataprovider of getAccountResult.value)
      accountEntities.push(await this.getAccountEntity(accountDataprovider))

    return accountEntities.map((accountEntity: Account) =>
      accountEntity.aggrateAccount()
    )
  }

  private async getAccountEntity (
    account: IAccountDataprovider
  ): Promise<Account> {
    const getTransactionsResult: Result<ITransactionProvider[], string> =
      await getAllAccountsTransactions(
        account.acc_number,
        getAccountTransactions
      )
    if (!getTransactionsResult.success)
      throw new Error("Error during transaction extraction")

    return new Account(
      mapAccounDto(account),
      getTransactionsResult.value.map(mapTransactionDto)
    )
  }
}
