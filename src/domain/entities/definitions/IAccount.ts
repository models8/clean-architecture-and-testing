import { ITransaction } from "./ITransaction"

export interface IAccount {
  acc_number: string;
  amount: string;
  transactions: ITransaction[];
}
