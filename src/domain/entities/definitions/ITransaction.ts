export interface ITransaction {
  label: string;
  amount: string;
  currency: string;
}
