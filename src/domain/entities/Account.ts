import { IAccount } from "./definitions/IAccount"
import { ITransaction } from "./definitions/ITransaction"
import { Result } from "../../utility/Result"

export class Account {
  private _transactions: ITransaction[] = []

  constructor (private _account: IAccount, transactions: ITransaction[] = []) {
    this.addTransactions(transactions)
  }

  aggrateAccount (): IAccount {
    return "" as any
  }

  public addTransactions (transactions: ITransaction[]): Result<null, string> {
    return "" as any
  }
}
