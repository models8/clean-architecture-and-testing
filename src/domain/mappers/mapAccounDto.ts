import { IAccount as IAccountDataprovider } from "../../dataproviders/apis/bridgeapi.io/definitions/IAccountsResponse"
import { IAccount } from "../entities/definitions/IAccount"

export const mapAccounDto = ({
  acc_number,
  amount
}: IAccountDataprovider): IAccount => {
  return {
    acc_number: acc_number,
    amount: amount.toString(),
    transactions: []
  }
}
