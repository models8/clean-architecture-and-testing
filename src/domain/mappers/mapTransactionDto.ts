import { ITransaction as ITransactionDataprovider } from "../../dataproviders/apis/bridgeapi.io/definitions/IAcountsTransactionResponse"
import { ITransaction } from "../entities/definitions/ITransaction"

export const mapTransactionDto = ({
  label,
  amount,
  currency
}: ITransactionDataprovider): ITransaction => {
  return {
    label,
    amount: amount.toString(),
    currency
  }
}
