import { IAccountsResponse } from "../../../../../dataproviders/apis/bridgeapi.io/definitions/IAccountsResponse"

export const ACCOUNT_API_RESPONSE: IAccountsResponse = {
  accounts: [
    {
      acc_number: "0000008",
      amount: 500,
      currency: "EUR"
    },
    {
      acc_number: "0000009",
      amount: 210,
      currency: "EUR"
    },
    {
      acc_number: "0000001",
      amount: 50,
      currency: "EUR"
    }
  ],
  links: {
    self: "/accounts?page=4",
    next: ""
  }
}
