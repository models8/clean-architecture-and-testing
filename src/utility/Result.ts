export type Result<ScucessData, ErrorData = Error> =
  | { success: true; value: ScucessData }
  | { success: false; value: ErrorData }
