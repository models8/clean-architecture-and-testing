export const BRIDGEIO_API = {
  baseUrl: "https://dsague.fr",
  apiKey: process.env.BRIDGEIO_API_KEY || "lorem",
  paths: {
    health: "/health",
    applications: "/applications",
    accounts: "/accounts",
    transactions: (accountNumber: string | number) =>
      `/accounts/${accountNumber}/transactions`
  }
}
