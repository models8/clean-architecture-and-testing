import { ILinks } from "./ILinks"

export interface IAccountsResponse {
  accounts: IAccount[];
  links: ILinks;
}

export interface IAccount {
  acc_number: string;
  amount: number;
  currency: string;
}
