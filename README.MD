- Code documentation with `TsDoc`
- Separation of concerns
- Typescript `Result` type
- Unit testing
- Integration testing
- Using Nock to mock http calls ( it adds assertions on http requests )

Suggested improvements
- Separate integration testing and unit testing
- Separate tsconfig & jest.config configuration files so that ignore test files in the javascript build 
